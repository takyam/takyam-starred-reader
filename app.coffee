http = require('http')
#ss = require('socketstream')
config = require('config')
xml2js = require('xml2js')
Twit = require('twit')
cheerio = require('cheerio')
Encoder = require('node-html-encoder').Encoder
mongoose = require('mongoose')

Schema = mongoose.Schema

#mongoose.set('debug', ss.env isnt 'production')
mongoose.connect 'mongodb://localhost/TakyamStarredReader'

encoder_ent = new Encoder('entity')
encoder_num = new Encoder('numerical')

#twitter settings
twitter = new Twit
  consumer_key: config.Twitter.consumer_key
  consumer_secret: config.Twitter.consumer_secret
  access_token: config.Twitter.access_token
  access_token_secret: config.Twitter.access_token_secret

#short URL (t.co) の長さをTwitterAPIから取得
short_url_length = 22
update_short_url_length = ->
  twitter.get('help/configuration', (err, reply)->
    short_url_length = reply.short_url_length_https + 1 if reply?.short_url_length_https?
  )
update_short_url_length()
setInterval(update_short_url_length, 1000 * 60 * 60)

INTERVAL = 1000 * 60 * 10 #5min

START_TIME = (new Date()).time

#ss.client.define 'main'
#  view: 'app.html'
#  css:  ['app.less']
#  code: ['libs/jquery.min.js', 'app']
#  tmpl: '*'
#
#ss.http.route '/', (req, res)->
#  res.serveClient('main')
#
#ss.client.formatters.add require('ss-coffee')
#ss.client.formatters.add require('ss-less')
#ss.client.templateEngine.use require('ss-hogan')
#
#ss.client.packAssets() if ss.env is 'production'
#
#server = http.Server ss.http.middleware
#server.listen 3000
#
#ss.start server


# update feed data
FeedSchema = new Schema
  id: String
  title: String
  content: String
  author: String
  category: [String]
  link: String
  published: Date
  updated: Date
  created_at: {type: Date, default: Date.now }
  updated_at: {type: Date, default: Date.now }
FeedSchema.pre 'save', (next)->
  @updated_at = (new Date()).getTime()
  next()
mongoose.model 'Feed', FeedSchema
Feed = mongoose.model('Feed')

EMPTY_STRING = '(empty)'
EMPTY_DATE = new Date(0)

parser = new xml2js.Parser()
options = config.GoogleReader.options

share = (feed)->
  number = 140 - short_url_length + 1 #+1はURLとタイトルの間のスペース分
  message = encoder_num.htmlDecode(
    encoder_ent.htmlDecode(
      feed.title.replace(/\r\n|\r|\n|\n\r/gm,'').replace(/<([^>])*?>/gm, '')
    )
  ).substr(0, number) + ' ' + feed.link
  twitter.post('statuses/update', {status: message}, (err, res)->
    if err isnt null
      console.log('tweet error', err)
    else
      console.log 'tweeted', message
  )

load= (continuation = null)->
  continuation = if continuation is null then '' else '?c=' + continuation
  req_option = {
    host: options.host
    path: options.path + continuation
    port: parseInt(options.port, 10)
    method: options.method
  }

  rss = ''
  http.get(req_option, (res)->
    res.on('data', (chunk)->
      rss+=chunk
    )
    res.on('end', ->
      parser.parseString(rss, (err, obj)->
        if err isnt null or !(obj?.feed?.entry?)
          console.log 'ERROR', err, obj
          return
        entry_length = obj.feed.entry.length
        entries = []
        ids = []
        has_after_entry = false

        for entry in obj.feed.entry
          #find and save entries
          continue if !(entry.id?[0]?._?) or !(entry.title?[0]?._?) or !(entry.link?[0]?.$?.href?)

          id = entry.id[0]._
          title = entry.title[0]._
          link = entry.link[0].$.href

          author = content = EMPTY_STRING
          published = updated = EMPTY_DATE

          content = entry.content[0]._ if entry.content?[0]?._?
          author = entry.author[0].name if entry.author?[0]?.name?

          categories = []
          if entry.category? and entry.category instanceof Array
            for category in entry.category
              categories.push category.$.term if category.$?.term?

          published = entry.published[0] if entry.published?[0]?
          updated = entry.updated[0] if entry.updated?[0]?

          entry_obj =
            id: id
            title: title
            content: content
            author: author
            category: categories
            link: link
            published: published
            updated: updated

          entries.push entry_obj
          ids.push id

          published_date = new Date(published)
          has_after_entry = true if published_date?.time? and published_date.time > START_TIME

        Feed.find({id: { $in: ids }}, (err, feeds)->
          return if err isnt null or !(feeds instanceof Array)

          for entry in entries
            Feed.findOneAndUpdate({id: entry.id}, entry, {upsert: true}, (err, saved_feed)->
              console.log 'ERROR', err if err isnt null
              is_new = true
              for feed in feeds
                if feed.id is saved_feed.id
                  is_new = false
                  break
              share(saved_feed) if is_new
            )

          if has_after_entry and feeds.length < entry_length and obj.feed['gr:continuation']?[0]?
            load(obj.feed['gr:continuation'][0])
        )
      )
    )
  ).on('error', (e)->
    console.log('err', e)
  )

load()

setInterval(load, INTERVAL)